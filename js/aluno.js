app.controller('aluno', [ '$scope', '$http', '$route','$location', '$timeout',
        function($scope, $http, $route, $location, $timeout) {
	
			$scope.reloadClass = '';
			//funçoes
			$scope.voltar = function() { 
				window.history.back();
			};
			$scope.select = function(index){
				$scope.selectedRow = index;
			};
	
			$scope.refresh = function() {
				$scope.reloadClass = 'fa-spin';
				$timeout(function(){
				   $http.get('rest/bwo').then(function(response) {
					   $scope.Permanencias = response.data;
				       $scope.reloadClass = '';
				   },function(){
					   $scope.reloadClass = '';
				   })
				},5000);
			};
			
			$scope.deleta = function(bwo){
				$http.post('rest/bwo/delete', permanencia).then(function(response) {
					var index = $scope.Permanencias.indexOf(permanencia);
					$scope.Permanencias.splice(index, 1);
				});
			};

			//Listas
			$scope.lista = [
				{id: 121, nome: "Ana Silva"},
				{id: 132, nome: "Julio Pereira"},
				{id: 143, nome: "Paula Costa"},
				{id: 631, nome: "Carlos Santos"},
				{id: 212, nome: "Joaquim Barbosa"},
				{id: 234, nome: "Lula da Silva"},
				{id: 666, nome: "Dilma Rousseff"},

			];
    		
			$scope.problemas = [
				{id: 1, nome: "Cefaléia", tipo: "number", atributo: "display: block", placeholder: "Frequência"},
				{id: 2, nome: "Lombalgia", tipo: "number", atributo: "display: block", placeholder: "Frequência"},
				{id: 3, nome: "Depressão", tipo: "number", atributo: "display: block", placeholder: "Frequência"},
				{id: 4, nome: "Asma/falta de ar", tipo: "number", atributo: "display: block", placeholder: "Frequência"},
				{id: 5, nome: "Náusea", tipo: "number", atributo: "display: block", placeholder: "Frequência"},
				{id: 6, nome: "Labirintite", tipo: "number", atributo: "display: block", placeholder: "Frequência"},
				{id: 7, nome: "Diabetes", tipo: "number", atributo: "display: block", placeholder: "Frequência"},
				{id: 8, nome: "Alergia", tipo: "text", atributo: "display: block", placeholder: "Qual?"},
			];
			$scope.problemas2 = [
				{id: 9, nome: "Sob recomendação médica", tipo: "hidden", atributo: "display: none", placeholder: ""},
				{id: 10, nome: "Dor no peito", tipo: "number", atributo: "display: block", placeholder: "Frequência"},
				{id: 11, nome: "Usa medicamento controlado", tipo: "text", atributo: "display: block", placeholder: "Qual?"},
				{id: 12, nome: "Hipertenso", tipo: "hidden", atributo: "display: none", placeholder: ""},
				{id: 13, nome: "Anêmico", tipo: "hidden", atributo: "display: none", placeholder: ""},
				{id: 14, nome: "Distúrbio muscular", tipo: "text", atributo: "display: block", placeholder: "Qual?"},
				{id: 15, nome: "Distúrbio esquelético", tipo: "text", atributo: "display: block", placeholder: "Qual?"},
				{id: 16, nome: "Distúrbio ligamentar", tipo: "text", atributo: "display: block", placeholder: "Qual?"},
				{id: 17, nome: "Desvio de postura", tipo: "text", atributo: "display: block", placeholder: "Qual?"},
				{id: 18, nome: "Outros", tipo: "text", atributo: "display: block", placeholder: "Qual?"},
			];
			$scope.habitosList = [
				{id: 1, nome: "Fuma", tipo: "number", atributo: "display: block", placeholder: "Frequência"},
				{id: 2, nome: "Bebe", tipo: "number", atributo: "display: block", placeholder: "Frequência"},
				{id: 3, nome: "Exercícios físicos", tipo: "number", atributo: "display: block", placeholder: "Frequência"},
				{id: 4, nome: "Ingestão de líquidos", tipo: "number", atributo: "display: block", placeholder: "Frequência"},
			];
			$scope.habitosList2 = [
				{id: 5, nome: "Bom sono", tipo: "hidden", atributo: "display: none", placeholder: ""},
				{id: 6, nome: "Controle nutricional", tipo: "hidden", atributo: "display: none", placeholder: ""},
				{id: 7, nome: "Esteróides e anabolizantes", tipo: "number", atributo: "display: block", placeholder: "Frequência"},
				{id: 8, nome: "Outros hábitos", tipo: "text", atributo: "display: block", placeholder: "Quais?"},
			];

			$scope.verTreinos = [
				{id:1, status: "material-icons green-text", nome: "Treino Para Deixar de Ser Frango 1º Mês"},
				{id:2, status: "material-icons orange-text", nome: "Treino Fica Grande Biiiiiiir 2º e 3º Mês"},
				{id:3, status: "material-icons red-text", nome: "Treino Para Bombar #vemmonstro 4º a 8º Mês"},
				{id:4, status: "material-icons grey-text", nome: "Treino para Crescer e Trincar os Músculos"},
			];
			$scope.verFichas = [
				{id:1, status: "material-icons green-text", nome: "Ficha Segunda-Feira"},
				{id:2, status: "material-icons green-text", nome: "Ficha Terça-Feira"},
				{id:3, status: "material-icons green-text", nome: "Ficha Quarta-Feira"},
				{id:4, status: "material-icons green-text", nome: "Ficha Quinta-Feira"},
				{id:5, status: "material-icons green-text", nome: "Ficha Sexta-Feira"},
				{id:6, status: "material-icons green-text", nome: "Ficha Sabado-Feira"}
			];

			$scope.verTreinos = [
				{id:1, status: "material-icons green-text", nome: "Treino Vem monxtro"},
				{id:2, status: "material-icons grey-text", nome: "Treino bambam"},
				{id:3, status: "material-icons grey-text", nome: "Treino batata-doce e frango"},
				{id:4, status: "material-icons red-text", nome: "Treino marombar"},
				{id:5, status: "material-icons red-text", nome: "Treino Estalone"},
				{id:6, status: "material-icons orange-text", nome: "Treino Schwarzenegger"}
			];

			$scope.verExercicios = [
				{id:"1", exercicio: "LegPress", grupo: "Pernas", serie: "3", repeticao: "15", peso: "80"},
				{id:"2", exercicio: "Cadeira Adutora", grupo: "Pernas", serie: "2", repeticao: "30", peso: "120"},
				{id:"3", exercicio: "Panturrilha no Hack", grupo: "Pernas", serie: "4", repeticao: "12", peso: "30"},
				{id:"4", exercicio: "Agachamento", grupo: "Pernas", serie: "3", repeticao: "15", peso: "20"},
				{id:"5", exercicio: "Rosca direta", grupo: "Biceps", serie: "3", repeticao: "12", peso: "45"},
				{id:"6", exercicio: "Rosca alternada", grupo: "Biceps", serie: "3", repeticao: "12", peso: "45"},
				{id:"7", exercicio: "Abdominal oblíquo", grupo: "Abdominal", serie: "3", repeticao: "12"}
			];
			
			var grupoIndexado = [];
			
			$scope.exerciciosAfiltrar = function() {
				grupoIndexado = [];
				return $scope.verExercicios;
			}
			
			$scope.filtroGrupo = function(x) {
				var grupoNovo = grupoIndexado.indexOf(x.grupo) == -1;
				if (grupoNovo) {
					grupoIndexado.push(x.grupo);
				}
				return grupoNovo;
			}

			//Funcoes de gerencia de alunos
			$scope.cadastra = function() {
				$location.path('/cadastrar');
			};

			$scope.listarFichas = function() {
				$location.path('/listaFichas');
			};

			$scope.vincula = function() {
				$location.path('/vincular')
			};

			$scope.listaAluno = function(nome) {
				$location.path('/cadastrar/:nome');
			};

			//Funcoes anamnese
			$scope.anamnese = function() {
				$location.path('/anamnese')
			};

			$scope.circunferencias = function() {
				$location.path('/circunferencias')
			};

			$scope.dobras = function() {
				$location.path('/dobras')
			};

			$scope.habitos = function() {
				$location.path('/habitos')
			};

			$scope.infoBasico = function() {
				$location.path('/infoBasico')
			};

			$scope.aspectosClinicos = function() {
				$location.path('/aspectosClinicos')
			};

			//Dashboard do aluno
			$scope.funcoesAluno = function() {
				$location.path('/funcoesAluno')
			};

			//PÁGINAS DE CADASTRO DE EXERCICIOS
			$scope.exerciciosCoxasPernas = function() {
				$location.path('/exerciciosCoxasPernas')
			};

			$scope.exerciciosPeitorais = function() {
				$location.path('/exerciciosPeitorais')
			};

			$scope.exerciciosBiceps = function() {
				$location.path('/exerciciosBiceps')
			};

			$scope.exerciciosDeltoide = function() {
				$location.path('/exerciciosDeltoide')
			};

			$scope.exerciciosTriceps = function() {
				$location.path('/exerciciosTriceps')
			};

			$scope.exerciciosDorsais = function() {
				$location.path('/exerciciosDorsais')
			};

			$scope.exerciciosGluteo = function() {
				$location.path('/exerciciosGluteo')
			};

			$scope.exerciciosAbdominais = function() {
				$location.path('/exerciciosAbdominais')
			};

			$scope.exerciciosAerobicos = function() {
				$location.path('/exerciciosAerobicos')
			};

			//FUNCOES DA DASHBOARD DO ALUNO
				//FICHA DE TREINO DO ALUNO
				$scope.fichaExercicio = function() {
					$location.path('/fichaExercicio')
				};
				//IMC
				$scope.imcAluno = function() {
					$location.path('/imcAluno')
				};
				//EVOLUÇÃO	
				$scope.evolucao = function() {
					$location.path('/evolucao')
				};
				//EDITAR ALUNO
				$scope.alterar = function(){
					$location.path('/alterarAluno');
				};
		} ]);
