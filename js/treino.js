app.controller('treino', [ '$scope', '$http', '$route','$location', '$timeout',
        function($scope, $http, $route, $location, $timeout) {
	
			$scope.reloadClass = '';
			//funçoes
			$scope.voltar = function() { 
				window.history.back();
			};
			$scope.select = function(index){
				$scope.selectedRow = index;
			};
	
			//Listas
			$scope.grupos = [
				{id: "coxaPerna", nome: "Coxa/Perna"},
				{id: "biceps", nome: "Bíceps"},
				{id: "triceps", nome: "Tríceps"},
				{id: "peitoral", nome: "Peitoral"},
				{id: , nome: },
				{id: , nome: },
				{id: , nome: },

			];
    		
			$scope.coxaPernaList = [
				{id: "coxaPerna1", nome: "coxaPerna1", atributo: "display: block;"},
				{id: "coxaPerna2", nome: "coxaPerna2", atributo: "display: block" },
				{id: "coxaPerna3", nome: "coxaPerna3", atributo: "display: block;"},
				{id: "coxaPerna4", nome: "coxaPerna4", atributo: "display: block;"},
				{id: "coxaPerna5", nome: "coxaPerna5", atributo: "display: block;"},
				{id: "coxaPerna6", nome: "coxaPerna6", atributo: "display: block;"},
				{id: "coxaPerna7", nome: "coxaPerna7", atributo: "display: block;"},
			];
			$scope.bicepsList = [
				{id: "biceps1", nome: "biceps1", atributo: "display: block"},
				{id: "biceps2", nome: "biceps2", atributo: "display: block"},
				{id: "biceps3", nome: "biceps3", atributo: "display: block"},
				{id: "biceps4", nome: "biceps4", atributo: "display: block"},
				{id: "biceps5", nome: "biceps5", atributo: "display: block"},

			];

		} ]);
