app.controller('usuario', [ '$scope', '$http', '$route','$location', '$timeout',
        function($scope, $http, $route, $location, $timeout) {
	
			//Funçoes da dashboard		
			$scope.usuario = function() {
				$location.path('/painel');
			};
			
    		$scope.instrutor = function(){
				$location.path('/instrutor');
			};
			
			$scope.alunos = function(){
				$location.path('/alunos');
			};
			
			$scope.voltar = function() { 
				window.history.back();
			};

			//CHAMA A DASHBOARD			
			$scope.gerenciarAluno = function() {
				$location.path('/gerenciarAluno')
			};
			$scope.dashAcademia = function() {
				$location.path('/dashAcademia')
			}
			$scope.dashAluno = function() {
				$location.path('/dashAluno')
			}
			$scope.dashInstrutor = function() {
				$location.path('/dashInstrutor')
			}

			//Funçoes da ficha
			$scope.listarExercicio = function() {
				$location.path('/listarExercicio')
			};

			$scope.voltar = function() { 
				window.history.back();
			};
			
			//Login do Usuario
			$scope.login = function() {
				$location.path('/login');
			}
			
			//Treinos
			$scope.treino = function(){
				$location.path('/treino');
			}
			
			$scope.verFicha = function(){
				$location.path('/verFicha');
			}
			$scope.verTreino = function(){
				$location.path('/verTreino');
			}
			$scope.verExercicio = function(){
				$location.path('/verExercicio');
			}
			$scope.montaTreino2 = function() {
				$location.path('/montaTreino2');
			};

		} ]);
