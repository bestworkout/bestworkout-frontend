app.controller('instrutor', [ '$scope', '$http', '$route','$location', '$timeout',
        function($scope, $http, $route, $location, $timeout) {
	
			//Funçoes da dashboard		
			$scope.montarTreino = function() {
				$location.path('/montaTreino');
			};

			$scope.listaInstrutor = [
				{id: 1, nome: "Paulo", email: "paulo@gmail.com"},
				{id: 2, nome: "Julia", email: "Julia@gmail.com"},
				{id: 3, nome: "Jorge", email: "Jorge@gmail.com"},
				{id: 4, nome: "Carlos", email: "carlos13@gmail.com"}
			];

			//Cadastro do instrutor

			$scope.cadastraInstrutor = function() {
				$location.path('/cadastroInstrutor')
			};
			
			$scope.alterarCadastroInstrutor = function() {
				$location.path('/alterarCadastroInstrutor')
			};

			$scope.select = function(index){
				$scope.selectedRow = index;
			};

			$scope.voltar = function() { 
				window.history.back();
			};

			$scope.montaTreino2 = function() {
				$location.path('/montaTreino2');
			};




			//Exercícios

			$scope.coxaPernaList = [
				{id: "Leg Press", nome: "Leg Press", atributo: "display: block;", serie: "serie1", rep: "rep1", peso: "peso1"},
				{id: "Cadeira extensora", nome: "Cadeira extensora", atributo: "display: block;", serie: "serie2", rep: "rep2", peso: "peso2"},
				{id: "Cadeira flexora", nome: "Cadeira flexora", atributo: "display: block;", serie: "serie3", rep: "rep3", peso: "peso3"},
				{id: "Mesa flexora", nome: "Mesa flexora", atributo: "display: block;", serie: "serie4", rep: "rep4", peso: "peso4"},
				{id: "Hack machine", nome: "Hack machine", atributo: "display: block;", serie: "serie5", rep: "rep5", peso: "peso5"},
				{id: "Cadeira adutora", nome: "Cadeira adutora", atributo: "display: block;", serie: "serie6", rep: "rep6", peso: "peso6"},
				{id: "Cadeira abdutora", nome: "Cadeira abdutora", atributo: "display: block;", serie: "serie7", rep: "rep7", peso: "peso7"},
				{id: "Panturrilha no hack", nome: "Panturrilha no hack", atributo: "display: block;", serie: "serie7", rep: "rep7", peso: "peso7"},
				{id: "Panturrilha horizontal", nome: "Panturrilha horizontal", atributo: "display: block;", serie: "serie7", rep: "rep7", peso: "peso7"},
				{id: "Panturrilha vertical", nome: "Panturrilha vertical", atributo: "display: block;", serie: "serie7", rep: "rep7", peso: "peso7"},
				{id: "Panturrilha Leg Press", nome: "Panturrilha Leg Press", atributo: "display: block;", serie: "serie7", rep: "rep7", peso: "peso7"},
				{id: "Agachamento", nome: "Agachamento", atributo: "display: block;", serie: "serie7", rep: "rep7", peso: "peso7"},
				{id: "Avanço", nome: "Avanço", atributo: "display: block;", serie: "serie7", rep: "rep7", peso: "peso7"},
				{id: "Afundo", nome: "Afundo", atributo: "display: block;", serie: "serie7", rep: "rep7", peso: "peso7"},
			];


			$scope.bicepsList = [
				{id: "Rosca Scott", nome: "Rosca Scott", atributo: "display: block;", serie: "serie1", rep: "rep1", peso: "peso1"},
				{id: "Rosca direta", nome: "Rosca direta", atributo: "display: block;", serie: "serie2", rep: "rep2", peso: "peso2"},
				{id: "Rosca invertida", nome: "Rosca invertida", atributo: "display: block;", serie: "serie3", rep: "rep3", peso: "peso3"},
				{id: "Rosca martelo", nome: "Rosca martelo", atributo: "display: block;", serie: "serie4", rep: "rep4", peso: "peso4"},
				{id: "Rosca alternada", nome: "Rosca alternada", atributo: "display: block;", serie: "serie5", rep: "rep5", peso: "peso5"},
				{id: "Rosca concentrada", nome: "Rosca concentrada", atributo: "display: block;", serie: "serie6", rep: "rep6", peso: "peso6"},
				{id: "Rosca máquina", nome: "Rosca máquina", atributo: "display: block;", serie: "serie7", rep: "rep7", peso: "peso7"},
				{id: "Rosca cristo", nome: "Rosca cristo", atributo: "display: block;", serie: "serie7", rep: "rep7", peso: "peso7"},
				{id: "Rosca unilateral", nome: "Rosca unilateral", atributo: "display: block;", serie: "serie7", rep: "rep7", peso: "peso7"},
			];
			

			$scope.peitoralList = [
				{id: "Supino Reto ", nome: "Supino Reto", atributo: "display: block;", serie: "serie1", rep: "rep1", peso: "peso1"},
				{id: "Supino inclinado", nome: "Supino inclinado", atributo: "display: block;", serie: "serie2", rep: "rep2", peso: "peso2"},
				{id: "Supino declinado", nome: "Supino declinado", atributo: "display: block;", serie: "serie3", rep: "rep3", peso: "peso3"},
				{id: "Voador", nome: "Voador", atributo: "display: block;", serie: "serie4", rep: "rep4", peso: "peso4"},
				{id: "Crussifixo ", nome: "Crussifixo ", atributo: "display: block;", serie: "serie5", rep: "rep5", peso: "peso5"},
				{id: "Crussifixo voador", nome: "peitoral6", atributo: "display: block;", serie: "serie6", rep: "rep6", peso: "peso6"},
				{id: "Crussifixo inclinado", nome: "Crussifixo inclinado", atributo: "display: block;", serie: "serie7", rep: "rep7", peso: "peso7"},
				{id: "Crussifixo declinado", nome: "Crussifixo declinado", atributo: "display: block;", serie: "serie7", rep: "rep7", peso: "peso7"},
				{id: "Crossover", nome: "Crossover", atributo: "display: block;", serie: "serie7", rep: "rep7", peso: "peso7"},
				{id: "Apoio", nome: "Apoio", atributo: "display: block;", serie: "serie7", rep: "rep7", peso: "peso7"},
				
			];

			$scope.deltoideList = [
				{id: "Remada alta", nome: "Remada alta", atributo: "display: block;", serie: "serie1", rep: "rep1", peso: "peso1"},
				{id: "Elevação lateral", nome: "Elevação lateral", atributo: "display: block;", serie: "serie2", rep: "rep2", peso: "peso2"},
				{id: "Desenvolvimento ombro", nome: "Desenvolvimento ombro", atributo: "display: block;", serie: "serie3", rep: "rep3", peso: "peso3"},
			];

			$scope.tricepsList = [
				{id: "Triceps testa", nome: "Triceps testa", atributo: "display: block;", serie: "serie1", rep: "rep1", peso: "peso1"},
				{id: "Triceps paralela", nome: "Triceps paralela", atributo: "display: block;", serie: "serie2", rep: "rep2", peso: "peso2"},
				{id: "Triceps francês", nome: "Triceps francês", atributo: "display: block;", serie: "serie3", rep: "rep3", peso: "peso3"},
				{id: "Triceps corda", nome: "Triceps corda", atributo: "display: block;", serie: "serie4", rep: "rep4", peso: "peso4"},
				{id: "Triceps banco", nome: "Triceps banco", atributo: "display: block;", serie: "serie5", rep: "rep5", peso: "peso5"},
				{id: "Triceps polia", nome: "Triceps polia", atributo: "display: block;", serie: "serie6", rep: "rep6", peso: "peso6"},
				{id: "Triceps supinado", nome: "Triceps supinado", atributo: "display: block;", serie: "serie7", rep: "rep7", peso: "peso7"},
				{id: "Triceps coice", nome: "Triceps coice", atributo: "display: block;", serie: "serie7", rep: "rep7", peso: "peso7"},
			];

			$scope.dorsalList = [
				{id: "Puxada alta", nome: "Puxada alta", atributo: "display: block;", serie: "serie1", rep: "rep1", peso: "peso1"},
				{id: "Puxada supinada", nome: "Puxada supinada", atributo: "display: block;", serie: "serie2", rep: "rep2", peso: "peso2"},
				{id: "Remada com barra", nome: "Remada com barra", atributo: "display: block;", serie: "serie3", rep: "rep3", peso: "peso3"},
				{id: "Remada hammer", nome: "Remada hammer", atributo: "display: block;", serie: "serie4", rep: "rep4", peso: "peso4"},
				{id: "Remada baixa", nome: "Remada baixa", atributo: "display: block;", serie: "serie5", rep: "rep5", peso: "peso5"},
				{id: "Remada com halter", nome: "Remada com halter", atributo: "display: block;", serie: "serie6", rep: "rep6", peso: "peso6"},
				{id: "Encolhimento barra", nome: "Encolhimento barra", atributo: "display: block;", serie: "serie7", rep: "rep7", peso: "peso7"},
				{id: "Encolhimento halter", nome: "Encolhimento halter", atributo: "display: block;", serie: "serie7", rep: "rep7", peso: "peso7"},
			];

			$scope.gluteoList = [
				{id: "Gluteo máquina", nome: "Gluteo máquina", atributo: "display: block;", serie: "serie1", rep: "rep1", peso: "peso1"},
				{id: "Agachamento terra", nome: "Agachamento terra", atributo: "display: block;", serie: "serie2", rep: "rep2", peso: "peso2"},
				{id: "Agachamento stiff", nome: "Agachamento stiff", atributo: "display: block;", serie: "serie3", rep: "rep3", peso: "peso3"},
				
			];

			$scope.abdominalList = [
				{id: "Abdominal remador ", nome: "Abdominal remador", atributo: "display: block;", serie: "serie1", rep: "rep1", peso: "peso1"},
				{id: "Abdominal rolinho", nome: "Abdominal rolinho", atributo: "display: block;", serie: "serie2", rep: "rep2", peso: "peso2"},
				{id: "Abdominal infra", nome: "Abdominal infra", atributo: "display: block;", serie: "serie3", rep: "rep3", peso: "peso3"},
				{id: "Abdominal supra", nome: "Abdominal supra", atributo: "display: block;", serie: "serie4", rep: "rep4", peso: "peso4"},
				{id: "Abdominal oblíquo", nome: "Abdominal oblíquo", atributo: "display: block;", serie: "serie5", rep: "rep5", peso: "peso5"},
				{id: "Abdominal tesoura", nome: "Abdominal tesoura", atributo: "display: block;", serie: "serie6", rep: "rep6", peso: "peso6"},
				{id: "Abdominal semi-canivete", nome: "Abdominal semi-canivete", atributo: "display: block;", serie: "serie7", rep: "rep7", peso: "peso7"},
				{id: "Abdominal em V", nome: "Abdominal em V", atributo: "display: block;", serie: "serie7", rep: "rep7", peso: "peso7"},
			];

			$scope.aerobicoList = [
				{id: "Esteira", nome: "Esteira", atributo: "display: block;", temp: "temp1", intens: "intens1"},
				{id: "Bike", nome: "Bike", atributo: "display: block;", temp: "temp2", intens: "intens2"},
				{id: "Transport", nome: "Transport", atributo: "display: block;", temp: "temp3", intens: "intens3"},
				
			];

			$scope.verFichas = [
				{id:1, status: "material-icons green-text", nome: "Ficha Segunda-Feira"},
				{id:2, status: "material-icons green-text", nome: "Ficha Terça-Feira"},
				{id:3, status: "material-icons green-text", nome: "Ficha Quarta-Feira"},
				{id:4, status: "material-icons green-text", nome: "Ficha Quinta-Feira"},
				{id:5, status: "material-icons green-text", nome: "Ficha Sexta-Feira"},
				{id:6, status: "material-icons green-text", nome: "Ficha Sabado-Feira"}
			];

			$scope.ExercicioAdd = function() {
				$location.path('/montaFicha');
			};

			$scope.verExercicios = [
				{id:"1", exercicio: "LegPress", grupo: "Pernas", serie: "3", repeticao: "15", peso: "80"},
				{id:"2", exercicio: "Cadeira Adutora", grupo: "Pernas", serie: "2", repeticao: "30", peso: "120"},
				{id:"3", exercicio: "Panturrilha no Hack", grupo: "Pernas", serie: "4", repeticao: "12", peso: "30"},
				{id:"4", exercicio: "Agachamento", grupo: "Pernas", serie: "3", repeticao: "15", peso: "20"},
				{id:"5", exercicio: "Rosca direta", grupo: "Biceps", serie: "3", repeticao: "12", peso: "45"},
				{id:"6", exercicio: "Rosca alternada", grupo: "Biceps", serie: "3", repeticao: "12", peso: "45"},
				{id:"7", exercicio: "Abdominal oblíquo", grupo: "Abdominal", serie: "3", repeticao: "12"}
			];

			var grupoIndexado = [];
			
			$scope.exerciciosAfiltrar = function() {
				grupoIndexado = [];
				return $scope.verExercicios;
			};
			
			$scope.filtroGrupo = function(x) {
				var grupoNovo = grupoIndexado.indexOf(x.grupo) == -1;
				if (grupoNovo) {
					grupoIndexado.push(x.grupo);
				}
				return grupoNovo;
			};

			$scope.criarFicha = function() {
				$location.path('/criaFicha');
			};

		}
]);