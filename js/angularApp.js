var app = angular.module('bwo', [ 'ngMask', 'ngRoute', 'ngMaterial', 'ngMessages', 'ngAria']);

app.config([ '$routeProvider', function($routeProvider) {
	$routeProvider.otherwise({
		redirectTo : '/'
	})
	/*  NOVO CONTROLADOR PARA A DASHBOARD  */
	.when('/', {
		templateUrl : 'templates/usuario/dashboard.html',
		controller : 'usuario', 
	}).when('/alunos', {
		templateUrl : 'templates/aluno/listaAluno.html',
		controller : 'funcoesGerais',
	}).when('/instrutor/:instrutorNome', {
		templateUrl : 'templates/listaAluno.html',
		controller : 'alunoEspec'
	}).when('/logar', {
		templateUrl : '',
		controller : 'Login'
	}).when('/cadastrar', {
		templateUrl : 'templates/aluno/cadastroAluno.html',
		controller : 'aluno'
	}).when('/painel', {
		templateUrl: 'templates/usuario/dashboard.html',
		controller: 'funcoesGerais'
	}).when('/instrutor', {
		templateUrl : 'templates/instrutor/listaInstrutor.html',
		controller : 'funcoesGerais'
	}).when('/cadastroInstrutor', {
		templateUrl : 'templates/instrutor/cadastroInstrutor.html',
		controller : 'instrutor'
	}).when('/alterarCadastroInstrutor', {
		templateUrl : 'templates/instrutor/alterarCadastroInstrutor.html',
		controller : 'instrutor'	
	}).when('/circunferencias', {
		templateUrl : 'templates/aluno/clinico/circunferencias.html',
		controller : 'aluno'
	}).when('/resultadosAnamenese', {
		templateUrl : 'templates/aluno/evolucao.html',
		controller : 'aluno'
	}).when('/dobras', {
		templateUrl : 'templates/aluno/clinico/dobras.html',
		controller : 'aluno'
	}).when('/cintQuad', {
		templateUrl : 'templates/aluno/clinico/cintQuad.html',
		controller : 'aluno'
	}).when('/habitos', {
		templateUrl : 'templates/aluno/clinico/habitos.html',
		controller : 'aluno'
	}).when('/infoBasico', {
		templateUrl : 'templates/aluno/clinico/infoBasico.html',
		controller : 'aluno'
	}).when('/aspectosClinicos', {
		templateUrl : 'templates/aluno/clinico/aspectosClinicos.html',
		controller : 'aluno'
	}).when('/vincular', {
		templateUrl : 'templates/aluno/vinculo.html',
		controller : 'aluno'
	})

	//Listas de Treino
	.when('/listarExercicio', {
		templateUrl : 'templates/treino/listarExercicio.html',
		controller : 'usuario'
	})
	//Cadastro de Treino
	.when('/exerciciosCoxasPernas', {
		templateUrl : 'templates/exercicios/coxasPernas.html',
		controller : 'aluno'
	}).when('/exerciciosBiceps', {
		templateUrl : 'templates/exercicios/biceps.html',
		controller : 'aluno'
	}).when('/exerciciosPeitorais', {
		templateUrl : 'templates/exercicios/peitorais.html',
		controller : 'aluno'
	}).when('/exerciciosDeltoide', {
		templateUrl : 'templates/exercicios/deltoide.html',
		controller : 'aluno'
	}).when('/exerciciosTriceps', {
		templateUrl : 'templates/exercicios/triceps.html',
		controller : 'aluno'
	}).when('/exerciciosDorsais', {
		templateUrl : 'templates/exercicios/dorsais.html',
		controller : 'aluno'
	}).when('/exerciciosGluteo', {
		templateUrl : 'templates/exercicios/gluteo.html',
		controller : 'aluno'
	}).when('/exerciciosAbdominais', {
		templateUrl : 'templates/exercicios/abdominais.html',
		controller : 'aluno'
	}).when('/exerciciosAerobicos', {
		templateUrl : 'templates/exercicios/aerobicos.html',
		controller : 'aluno'
	})

	//funções instrutor
	.when('/montaTreino', {
		templateUrl : 'templates/treino/montaTreino.html',
		controller : 'instrutor'
	})

	// Dashboard aluno
	.when('/alterarAluno', {
		templateUrl : 'templates/aluno/editarAluno.html',
		controller : 'aluno'
	}).when('/funcoesAluno', {
		templateUrl : 'templates/aluno/funcoesAluno.html',
		controller : 'aluno'
	}).when('/fichaExercicio', {
		templateUrl : 'templates/treino/fichaExercicio.html',
		controller : 'aluno'
	}).when('/anamnese', {
		templateUrl : 'templates/aluno/anamneseTabs.html',
		controller : 'aluno'
	}).when('/imcAluno', {
		templateUrl : 'templates/aluno/clinico/imcAluno.html',
		controller : 'aluno'
	}).when('/treino', {
		templateUrl : 'templates/aluno/treinoQrcode.html',
		controller : 'aluno'
	}).when('/verFicha', {
		templateUrl : 'templates/aluno/verFichas.html',
		controller : 'aluno'
	}).when('/verTreino', {
		templateUrl : 'templates/aluno/verTreinos.html',
		controller : 'aluno'
	}).when('/verExercicio', {
		templateUrl : 'templates/aluno/verExercicios.html',
		controller : 'aluno'
	}).when('/montaTreino2', {
		templateUrl : 'templates/treino/montaTreino2.html',
		controller : 'usuario'
	}).when('/listaFichas', {
		templateUrl : 'templates/treino/listaFicha.html',
		controller : 'usuario'
	}).when('/montaFicha', {
		templateUrl : 'templates/treino/montaFicha.html',
		controller : 'instrutor'
	}).when('/criaFicha', {
		templateUrl : 'templates/treino/criaFicha.html',
		controller : 'instrutor'
	})
	
	
} ]);
